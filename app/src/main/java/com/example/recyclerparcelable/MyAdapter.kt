package com.example.recyclerparcelable

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.recyclerparcelable.data.Character
import com.example.recyclerparcelable.databinding.HolderPersonBinding

/**
 * Created by sergi on 02/03/2022.
 * Copyright (c) 2022 Qastusoft. All rights reserved.
 */

class MyAdapter() :
    RecyclerView.Adapter<MyAdapter.MyViewHolder>() {

//    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
//        val textView: TextView
//
//        init {
//            textView = view.findViewById(R.id.holder_name)
//        }
//
//        fun setMyData(jugador: String) {
//            textView.text = jugador
//            textView.setOnClickListener {
//                Toast.makeText(textView.context, "$jugador en posicion ${adapterPosition}", Toast.LENGTH_SHORT).show()
//            }
//        }
//    }

    private val dataList = ArrayList<Character>()

    inner class MyViewHolder(val binding: HolderPersonBinding) :
        RecyclerView.ViewHolder(binding.root) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = HolderPersonBinding.inflate(layoutInflater, parent, false)
        return MyViewHolder(binding)
    }


    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val jugador = dataList[position]

        with(holder.binding) {
            Glide.with(holder.itemView).load(jugador.images.main).into(holderImage)
            holderName.text = "${jugador.name.first} ${jugador.name.middle} ${jugador.name.last} "
            holderPlanet.text = jugador.homePlanet
        }

        holder.itemView.setOnClickListener {
            Navigation.findNavController(it)
                .navigate(R.id.action_FirstFragment_to_SecondFragment)
        }
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    fun add(item: Character) {
        dataList.add(item)
        notifyItemInserted(dataList.size)
    }

    fun remove() {
        dataList.removeAt(dataList.size - 1)
        notifyItemRemoved(dataList.size)
    }

    fun update(list: List<Character>) {
        dataList.clear()
        dataList.addAll(list)
        notifyDataSetChanged()
    }

}