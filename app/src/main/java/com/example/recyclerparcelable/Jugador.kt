package com.example.recyclerparcelable


data class Jugador(
    val nombre: String,
    val numero: Int
)
