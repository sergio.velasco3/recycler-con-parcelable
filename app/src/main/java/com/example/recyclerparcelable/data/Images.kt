package com.example.recyclerparcelable.data

import com.google.gson.annotations.SerializedName


/**
 * Created by sergi on 21/03/2023.
 * Copyright (c) 2023 Qastusoft. All rights reserved.
 */

data class Images(
    @SerializedName("head-shot")
    val headShot: String,
    val main: String
)
