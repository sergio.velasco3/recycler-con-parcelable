package com.example.recyclerparcelable.data


/**
 * Created by sergi on 20/03/2023.
 * Copyright (c) 2023 Qastusoft. All rights reserved.
 */

data class Character(
    val name: Name,
    val images: Images,
    val age: String,
    val gender: String,
    val species: String,
    val homePlanet: String,
    val occupation: String,
    val sayings: List<String>
)
