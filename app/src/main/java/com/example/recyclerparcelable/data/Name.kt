package com.example.recyclerparcelable.data


/**
 * Created by sergi on 21/03/2023.
 * Copyright (c) 2023 Qastusoft. All rights reserved.
 */

data class Name(
    val first: String,
    val middle: String,
    val last: String
)