package com.example.recyclerparcelable

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.recyclerparcelable.data.Repository
import com.example.recyclerparcelable.databinding.FragmentFirstBinding

class FirstFragment : Fragment() {

    private lateinit var binding: FragmentFirstBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = FragmentFirstBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val repository = Repository()

        val personajes = repository.getCharacters()

        val recyclerView = binding.recyclerview
        val llm = StaggeredGridLayoutManager( 2, RecyclerView.VERTICAL)
        recyclerView.layoutManager = llm
        val adapter = MyAdapter()
        recyclerView.adapter = adapter

        adapter.update(personajes)
    }
}